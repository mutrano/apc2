#include <stdio.h>

int main(void){
    int n,i,temp,j;
    scanf("%d",&n);
    int vet[n];
    for(i=0;i<n;i++){
        scanf("%d",&vet[i]);
    }
    for(i=0;i<n-1;i++){
        for(j=i+1;j<n;j++){
            if(vet[j]<vet[i]){
                temp=vet[i];
                vet[i]=vet[j];
                vet[j]=temp;
            }
        }
    }
    printf("Menor: %d\nMaior: %d\n",vet[0],vet[n-1]);
    return 0;
}