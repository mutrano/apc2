#include <stdio.h>

int main(void){
    double tj,d,rend;
    int t,i;
    scanf("%lf %lf %d",&d,&tj,&t);
    for(i=0;i<t;i++){
        rend=(d*tj)/100;
        d+=rend;
        printf("Rendimento no mes %d: %.2lf\n",i,rend);
    }
    printf("Saldo final do investimento: %.2lf\n",d);
    return 0;
}