#include <stdio.h>

int main(void){
    int diferencas=0,total=0,salario,flag=0;
    while(flag==0){
        scanf("%d",&salario);
        if(salario<=0){
            break;
        }
        else if(salario<500){
            diferencas+=100;
            total+=salario+100;
        }
        else if(salario>=500 && salario<=1000){
            diferencas+=75;
            total+=salario+75;
        }
        else{
            diferencas+=50;
            total+=salario+50;
        }
    }
    printf("%d\n%d\n",total,diferencas);
    return 0;
}