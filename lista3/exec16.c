#include <stdio.h>

int main(void){
    int id,op,qp=0;
    double p1=0,p2=0,p3=0,p4=0,p5=0,mi=0;

    scanf("%d",&id);
    while(id>=0){
        scanf("%d",&op);
        while(op<1 || op>5){
            scanf("%d",&op);
        }
        qp++;
        mi+=id;
        if(op==1){
            p1++;
        }
        if(op==2){
            p2++;
        }
        if(op==3){
            p3++;
        }
        if(op==4){
            p4++;
        }
        if(op==5){
            p5++;
        }
        scanf("%d",&id);
    }
    mi=mi/qp;
    p1=(p1/qp)*100;
    p2=(p2/qp)*100;
    p3=(p3/qp)*100;
    p4=(p4/qp)*100;
    p5=(p5/qp)*100;

    printf("qtde de participantes: %d\n",qp);
    printf("idade media dos participantes: %.1lf\n",mi);
    printf("Otimo: %.1lf%\n",p1);
    printf("Bom: %.1lf%\n",p2);
    printf("Regular: %.1lf%\n",p3);
    printf("Ruim: %.1lf%\n",p4);
    printf("Pessimo: %.1lf%\n",p5);
    return 0;
}