#include <stdio.h>

int main(void){
    int n,maior=-1,atual,i;
    scanf("%d",&n);
    for(i=0;i<n;i++){
        scanf("%d",&atual);
        if(atual>maior){
            maior=atual;
        }
    }
    if(maior<10){
        printf("1\n");
    }
    if(maior>=10 && maior<20){
        printf("2\n");
    }
    else{
        printf("3\n");
    }
    return 0;
}