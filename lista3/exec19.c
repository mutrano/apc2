#include <stdio.h>
#include <limits.h>

int main(void){
    int menor=INT_MAX,maior=INT_MIN,i,atual;
    for(i=0;i<5;i++){
        scanf("%d",&atual);
        if(atual<menor){
            menor=atual;
        }
        if(atual>maior){
            maior=atual;
        }
    }
    printf("Menor: %d\n",menor);
    printf("Maior: %d\n",maior);
    return 0;
}