#include <stdio.h>

int main(void){
    double base,topo,saida;
    int i;
    scanf("%lf %lf",&base,&topo);
    saida=base;
    for(i=1;i<topo;++i){
        saida*=base;
    }
    printf("%.1lf\n",saida);
    return 0;
}