#include <stdio.h>

int main(void){
    double deposito,juros,imposto,imp_pagar,final;
    scanf("%lf %lf %lf",&deposito,&juros,&imposto);
    imp_pagar=(deposito*juros*imposto)/10000;
    final=deposito + ((deposito*juros)/100);
    final=final - imp_pagar;
    printf("%.2lf %.2lf\n",imp_pagar,final);
    return 0;
}