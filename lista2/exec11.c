#include <stdio.h>

int main(void){
    double x,cubo;
    scanf("%lf",&x);
    if(x<=1){
        printf("1.0\n");
    }
    else if(x<=2){
        printf("2.0\n");
    }
    else if(x<=3){
        printf("%.1lf",x*x);
    }
    else{
        cubo=x;
        cubo=cubo*x*x;
        printf("%.1lf",cubo);
    }
    return 0;
}
