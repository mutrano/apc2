#include <stdio.h>

int main(void){
    float peso,altura,imc;
    scanf("%f %f",&peso,&altura);
    imc=peso/(altura*altura);
    if(imc<20){
        printf("ABAIXO\n");
    }
    else if(20<=imc && imc<25){
        printf("NORMAL\n");
    }
    else if(25<=imc && imc<30){
        printf("SOBREPESO\n");
    }
    else if(30<=imc && imc<40){
        printf("OBESO\n");
    }
    else{
        printf("MORBIDO\n");
    }
    printf("%f",imc);
    return 0;
}