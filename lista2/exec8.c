#include <stdio.h>

int main(void){
    double sm,credito;
    scanf("%lf",&sm);
    if(sm<=500){
        credito=sm*0.00;
    }
    else if(500<sm && sm<=1000){
        credito=sm*0.30;
    }
    else if(1000<sm && sm <=3000){
        credito=sm*0.40;
    }
    else{
        credito=sm*0.50;
    }
    printf("%.2lf\n",credito);
    return 0;
}