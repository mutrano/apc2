#include <stdio.h>
#include <math.h>

int main(void){
    char entrada;
    double a,b,c,saida;
    scanf("%c",&entrada);
    if(entrada=='+'){
        scanf("%lf %lf",&a,&b);
        saida=a+b;
        printf("%.3lf\n",saida);
    }
    else if(entrada=='-'){
        scanf("%lf %lf",&a,&b);
        saida=a-b;
        printf("%.3lf\n",saida);
    }
    else if(entrada=='*'){
        scanf("%lf %lf",&a,&b);
        saida=a*b;
        printf("%.3lf\n",saida);
    }
    else if(entrada=='/'){
        scanf("%lf %lf",&a,&b);
        if(b==0){
            printf("ERROR\n");
        }
        else{
            saida=a/b;
        }
        printf("%.3lf\n",saida);
    }
    else if(entrada=='%'){
        int d,e;
        scanf("%d %d",&d,&e);
        if(e==0){
            printf("ERROR\n");
        }
        else{
            printf("%d.000\n",d%e);
        }
    }
    else if(entrada=='p'){
        scanf("%lf %lf",&a,&b);
        if(a<0){
            b=(int)b;
        }
        saida=pow(a,b);
        printf("%.3lf\n",saida);
    }
    else if(entrada=='b'){
        scanf("%lf %lf %lf",&a,&b,&c);
        double delta,raiz1,raiz2;
        delta=pow(b,2)-(4*a*c);
        if(delta<0){
            printf("ERROR\n");
        }
        else if(delta==0){
            raiz1=(-b+sqrt(delta))/(2*a);
            printf("%.3lf %.3lf\n",raiz1, raiz1);
        }
        else{
            raiz1=(-b+sqrt(delta))/(2*a);
            raiz2=(-b-sqrt(delta))/(2*a);
            printf("%.3lf %.3lf\n",raiz1,raiz2);
        }
    }
    else{
        scanf("%lf",&a);
        if(a<0){
            printf("ERROR\n");
        }
        else{
        saida=sqrt(a);
        printf("%.3lf\n",saida);
        }
    }
    return 0;
}